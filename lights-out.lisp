;; lights-out.lisp
;; 
;; Implements a turn-off-all-lights game.
;; The player will be prompted on what light the player wants to toggle.
;; The player will be notified when all the lights are out.
;; 
;; Author: Tou Ko Lee


;; prints the board
(defun print-board (board)
    ;; Prints the first 5 numbers
    (if (= 0 (mod (length board) 5))
        (format t "~% ~a" (car board))
        (format t " ~a" (car board))
    )
    ;; if there is nothing to print return nil else recurses itself
    (if (null board)
        nil
        (print-board (rest board))
    )
)

;; toggles one light on the board
(defun toggle (board pos)
    ;; checks if position is within the board
    (if (= 0 pos)
        ;; reached the correct position to toggle board
        (if (zerop (car board))
            (cons 1 (cdr board))
            (cons 0 (cdr board))
        )
        ;; else adds a number
        (cons (car board) (toggle (cdr board) (- pos 1)))
    )
)

;; toggles all the lights needed
(defun toggle-lights (board pos)
    ;; (toggle-down (toggle board pos) pos)
    (toggle-left (toggle-right (toggle-up (toggle-down (toggle board pos) pos) pos) pos) pos)
)

;; toggles the light above the orginal position
(defun toggle-up (board pos)
    ;; checks if pos - 5 is within the board and if not toggles it
    (if (< (- pos 5) 0)
        board
        (toggle board (- pos 5))
    )
)

;; toggles the light below the orginal position
(defun toggle-down (board pos)
    ;; checks if pos + 5 is within the board and if not toggles it
    (if (> (+ pos 5) 24)
        board
        (toggle board (+ pos 5))
    )
)

;; toggles the light to the left of the orginal position
(defun toggle-left (board pos)
    ;; checks if pos is on the left edge and if not toggles the left light
    (if (= (mod pos 5) 0)
        board
        (toggle board (- pos 1))
    )
)

;; toggles the light to the right of the orginal position
(defun toggle-right (board pos)
    ;; checks if pos is on the right edge and if not toggles the right light
    (if (or (= pos 0) (not (= (mod (+ pos 1) 5) 0)))
        (toggle board (+ pos 1))
        board
    )
)

;; determines if all the lights are out
(defun is-lights-out-p (board)
    ;; returns true if board is all zero
    (if (null board)
        t
        ;; else iterates through board until a one is found or reaches the end
        (if (= (car board) 1)
            nil
            (is-lights-out-p (cdr board))
        )
    )
)

;; function to create a random board
(defun rand-board (board count)
    ;; makes a 5 x 5 board
    (if (= count 25)
        ;; checks if there is at least one 1 and returns the board
        (if (is-lights-out-p board)
            (rand-board nil 0)
            board
        )
        (rand-board (cons (random 2) board) (+ 1 count))
    )
)

;; Gets user input for what light position to toggle
(defun get-input (board message)
    ;; prints the message along with getting the input and checking if its valid
    (format t "~%~a" message)
    (let (
        ;; gets user input
        (pos (read))
        )
        ;; checks if pos is "q" to return it
        (if (or (equal pos "q") (equal pos "n") (equal pos "ai"))
            pos
            ;; checks if pos is within boundries
            (if (or (< pos 0) (> pos 24))
                (get-input board "The position should be between 0 and 24 inclusive. Enter again:")
                pos
            )
        )
    )
)

;; Gets AI input for what light position to toggle
(defun get-ai-input (board message)
    ;; prints the message along with getting the input and checking if its valid
    (format t "~%~a" message)
    ;; gets AI input
    (let (
        (pos (next board 0))
        )
        (princ pos)
        ;; returns pos
        pos
    )
)

;; AI function to pick a switch
(defun next (board pos)
    ;; there isn't a one in the board
    (if (null (nth pos board))
        nil
        ;; iterates through board until a one is found
        (if (= (nth pos board) 1)
            ;; checks if I can toggle down else we're at the bottom row and will need to compare states
            (if (< (+ 5 pos) 25)
                (+ 5 pos)
                (states board)
            )
            (next board (+ 1 pos))
        )
    )
)

;; checks what the last row is like and toggles the rigth light at the top row to complete the game
(defun states (board)
    ;; the states
    (let (
        (state1 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 0 0))
        (state2 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1))
        (state3 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 1 0))
        (state4 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1))
        (state5 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 0 1))
        (state6 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 0))
        (state7 '(0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1))
        )
        ;; goes through each state to toggle the light at top, else board is unsolvable
        (if (equal board state1)
            1 
            (if (equal board state2)
                2
                (if (equal board state3)
                    4 
                    (if (equal board state4)
                        0 
                        (if (equal board state5)
                            0 
                            (if (equal board state5)
                                0 
                                (if (equal board state6)
                                    0 
                                    (if (equal board state7)
                                        3
                                        -1
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )
    )
)

;; Using the user input, toggles the board and asks the user to continue until all lights are out
(defun guess (board message count)
    ;; prints the board
    (format t "~%Here's your board:")
    (print-board board)
    (let (
        (pos (get-input board message))
        )
        ;; checks if lights are out or more guesses are needed
        (if (is-lights-out-p board)
            (format T "~%All the lights are out after ~D guesses." count)
            ;; checks if the user has quit or not
            (if (equal pos "q")
                (format T "~%You have quit after ~D guesses." count)
                (if (equal pos "n")
                    (guess (toggle-lights board (get-ai-input board message)) "Toggle another light: " (+ 1 count))
                    (if (equal pos "ai")
                        (guess-ai (toggle-lights board (get-ai-input board message)) "Toggle another light: " (+ 1 count))
                        (guess (toggle-lights board pos) "Toggle another light: " (+ 1 count))
                    )
                )
            )
        )
    )
)

;; Using the AI input, toggles the board and asks the AI to continue until all lights are out
(defun guess-ai (board message count)
    ;; prints the board
    (format t "~%Here's the board:")
    (print-board board)
   (let (
        (pos (get-ai-input board message))
       )
        ;; checks if lights are out or more guesses are needed
        (if (null pos)
            (format T "~%All the lights are out after ~D guesses." count)
            ;; checks if the board is solvable or not
            (if (= pos -1)
                (format T "~%The board is unsolvable with this AI after ~D guesses." count)
                (guess-ai (toggle-lights board pos) "Toggle another light: " (+ 1 count))
            )   
        )
    )
)

;; driver function to start the game
(defun start ()
    ;; prints out description of game
    (format t "The goal of this game is toggle off all the lights (make the board all zeros).")
    (format t "~%Enter \"q\" with the quotations to quit the game.")
    (format t "~%Enter \"n\" with the quotations for the ai to pick the next switch.")
    (format t "~%Enter \"ai\" with the quotations for the ai to play the rest of the game for you.")
    ;; starts the game
    (guess (rand-board nil 0) "Please enter a number to toggle the light at that position: " 0)
)

;; driver function to watch ai play the game
(defun ai ()
    ;; prints out description of game
    (format t "You will watch an AI play this game")
    ;; starts the game
    (guess-ai (rand-board nil 0) "The AI will enter a number to toggle the light at that position: " 0)
)

;; testing function
(defun test()
    (princ "Testing print-board with (1 0 1 0 1 1 0 1 0):")
    (format t "~%")
    (print-board '(1 0 1 0 1 1 0 1 0))
    (format t "~%")
    (format t "~%")
    (princ "Testing print-board: (1 0 1 0 1 1 0 1 0 1 1 0 1 0 1):")
    (print-board '(1 0 1 0 1 1 0 1 0 1 1 0 1 0 1))
    (format t "~%")
    (format t "~%")

    (princ "Testing toggle: (1 0 1 0 1 1 0 1 0 1 1 0 1 0 1) 0")
    (print-board (toggle '(1 0 1 0 1 1 0 1 0 1 1 0 1 0 1) 0))
    (format t "~%")
    (format t "~%")
    (princ "Testing toggle: (1 0 1 0 1 1 0 1 0 1 1 0 1 0 1) 1")
    (print-board (toggle '(1 0 1 0 1 1 0 1 0 1 1 0 1 0 1) 1))
    (format t "~%")
    (format t "~%")

    (princ "Testing is-lights-out-p: (1 0 1 0 1 1 0 1 0 1 1 0 1 0 1)")
    (format t "~%")
    (princ (is-lights-out-p '(1 0 1 0 1 1 0 1 0 1 1 0 1 0 1)))
    (format t "~%")
    (format t "~%")
    (princ "Testing is-lights-out-p: (0 0 0 0 0)")
    (format t "~%")
    (princ (is-lights-out-p '(0 0 0 0 0)))
    (format t "~%")
    (format t "~%")
)