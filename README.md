﻿Summary:
This LISP application is a game where the player has to toggle all the lights off on the board. The game will prompt the user to enter a number to toggle a light. When all the lights are off, the number of guesses taken will be printed onto the screen.

Contents/files and folders:
There is only the lights-out.lisp file that stores the code for the game. The functions in the file include how the user will interact with the game and how the board functions.

How to use project:
The game will start when the driver function, (start), is entered. The game will start with a short description of what the goal of the game is, their board, and a prompt to enter a position of what light to toggle.
You may also watch an AI play the game when the driver function, (ai), is entered.